/**
 * @fileoverview gRPC-Web generated client stub for todoPackage
 * @enhanceable
 * @public
 */

// GENERATED CODE -- DO NOT EDIT!


/* eslint-disable */
// @ts-nocheck


import * as grpcWeb from 'grpc-web';

import * as packages_protos_todo_pb from '../../packages/protos/todo_pb';


export class TodoClient {
  client_: grpcWeb.AbstractClientBase;
  hostname_: string;
  credentials_: null | { [index: string]: string; };
  options_: null | { [index: string]: any; };

  constructor (hostname: string,
               credentials?: null | { [index: string]: string; },
               options?: null | { [index: string]: any; }) {
    if (!options) options = {};
    if (!credentials) credentials = {};
    options['format'] = 'text';

    this.client_ = new grpcWeb.GrpcWebClientBase(options);
    this.hostname_ = hostname;
    this.credentials_ = credentials;
    this.options_ = options;
  }

  methodDescriptorCreateTodo = new grpcWeb.MethodDescriptor(
    '/todoPackage.Todo/CreateTodo',
    grpcWeb.MethodType.UNARY,
    packages_protos_todo_pb.CreateTodoRequest,
    packages_protos_todo_pb.CreateTodoResponse,
    (request: packages_protos_todo_pb.CreateTodoRequest) => {
      return request.serializeBinary();
    },
    packages_protos_todo_pb.CreateTodoResponse.deserializeBinary
  );

  createTodo(
    request: packages_protos_todo_pb.CreateTodoRequest,
    metadata: grpcWeb.Metadata | null): Promise<packages_protos_todo_pb.CreateTodoResponse>;

  createTodo(
    request: packages_protos_todo_pb.CreateTodoRequest,
    metadata: grpcWeb.Metadata | null,
    callback: (err: grpcWeb.RpcError,
               response: packages_protos_todo_pb.CreateTodoResponse) => void): grpcWeb.ClientReadableStream<packages_protos_todo_pb.CreateTodoResponse>;

  createTodo(
    request: packages_protos_todo_pb.CreateTodoRequest,
    metadata: grpcWeb.Metadata | null,
    callback?: (err: grpcWeb.RpcError,
               response: packages_protos_todo_pb.CreateTodoResponse) => void) {
    if (callback !== undefined) {
      return this.client_.rpcCall(
        this.hostname_ +
          '/todoPackage.Todo/CreateTodo',
        request,
        metadata || {},
        this.methodDescriptorCreateTodo,
        callback);
    }
    return this.client_.unaryCall(
    this.hostname_ +
      '/todoPackage.Todo/CreateTodo',
    request,
    metadata || {},
    this.methodDescriptorCreateTodo);
  }

  methodDescriptorGetTodos = new grpcWeb.MethodDescriptor(
    '/todoPackage.Todo/GetTodos',
    grpcWeb.MethodType.UNARY,
    packages_protos_todo_pb.GetTodosRequest,
    packages_protos_todo_pb.GetTodosResponse,
    (request: packages_protos_todo_pb.GetTodosRequest) => {
      return request.serializeBinary();
    },
    packages_protos_todo_pb.GetTodosResponse.deserializeBinary
  );

  getTodos(
    request: packages_protos_todo_pb.GetTodosRequest,
    metadata: grpcWeb.Metadata | null): Promise<packages_protos_todo_pb.GetTodosResponse>;

  getTodos(
    request: packages_protos_todo_pb.GetTodosRequest,
    metadata: grpcWeb.Metadata | null,
    callback: (err: grpcWeb.RpcError,
               response: packages_protos_todo_pb.GetTodosResponse) => void): grpcWeb.ClientReadableStream<packages_protos_todo_pb.GetTodosResponse>;

  getTodos(
    request: packages_protos_todo_pb.GetTodosRequest,
    metadata: grpcWeb.Metadata | null,
    callback?: (err: grpcWeb.RpcError,
               response: packages_protos_todo_pb.GetTodosResponse) => void) {
    if (callback !== undefined) {
      return this.client_.rpcCall(
        this.hostname_ +
          '/todoPackage.Todo/GetTodos',
        request,
        metadata || {},
        this.methodDescriptorGetTodos,
        callback);
    }
    return this.client_.unaryCall(
    this.hostname_ +
      '/todoPackage.Todo/GetTodos',
    request,
    metadata || {},
    this.methodDescriptorGetTodos);
  }

  methodDescriptorGetTodo = new grpcWeb.MethodDescriptor(
    '/todoPackage.Todo/GetTodo',
    grpcWeb.MethodType.UNARY,
    packages_protos_todo_pb.GetTodoRequest,
    packages_protos_todo_pb.GetTodoResponse,
    (request: packages_protos_todo_pb.GetTodoRequest) => {
      return request.serializeBinary();
    },
    packages_protos_todo_pb.GetTodoResponse.deserializeBinary
  );

  getTodo(
    request: packages_protos_todo_pb.GetTodoRequest,
    metadata: grpcWeb.Metadata | null): Promise<packages_protos_todo_pb.GetTodoResponse>;

  getTodo(
    request: packages_protos_todo_pb.GetTodoRequest,
    metadata: grpcWeb.Metadata | null,
    callback: (err: grpcWeb.RpcError,
               response: packages_protos_todo_pb.GetTodoResponse) => void): grpcWeb.ClientReadableStream<packages_protos_todo_pb.GetTodoResponse>;

  getTodo(
    request: packages_protos_todo_pb.GetTodoRequest,
    metadata: grpcWeb.Metadata | null,
    callback?: (err: grpcWeb.RpcError,
               response: packages_protos_todo_pb.GetTodoResponse) => void) {
    if (callback !== undefined) {
      return this.client_.rpcCall(
        this.hostname_ +
          '/todoPackage.Todo/GetTodo',
        request,
        metadata || {},
        this.methodDescriptorGetTodo,
        callback);
    }
    return this.client_.unaryCall(
    this.hostname_ +
      '/todoPackage.Todo/GetTodo',
    request,
    metadata || {},
    this.methodDescriptorGetTodo);
  }

  methodDescriptorDeleteTodo = new grpcWeb.MethodDescriptor(
    '/todoPackage.Todo/DeleteTodo',
    grpcWeb.MethodType.UNARY,
    packages_protos_todo_pb.DeleteTodoRequest,
    packages_protos_todo_pb.DeleteTodoResponse,
    (request: packages_protos_todo_pb.DeleteTodoRequest) => {
      return request.serializeBinary();
    },
    packages_protos_todo_pb.DeleteTodoResponse.deserializeBinary
  );

  deleteTodo(
    request: packages_protos_todo_pb.DeleteTodoRequest,
    metadata: grpcWeb.Metadata | null): Promise<packages_protos_todo_pb.DeleteTodoResponse>;

  deleteTodo(
    request: packages_protos_todo_pb.DeleteTodoRequest,
    metadata: grpcWeb.Metadata | null,
    callback: (err: grpcWeb.RpcError,
               response: packages_protos_todo_pb.DeleteTodoResponse) => void): grpcWeb.ClientReadableStream<packages_protos_todo_pb.DeleteTodoResponse>;

  deleteTodo(
    request: packages_protos_todo_pb.DeleteTodoRequest,
    metadata: grpcWeb.Metadata | null,
    callback?: (err: grpcWeb.RpcError,
               response: packages_protos_todo_pb.DeleteTodoResponse) => void) {
    if (callback !== undefined) {
      return this.client_.rpcCall(
        this.hostname_ +
          '/todoPackage.Todo/DeleteTodo',
        request,
        metadata || {},
        this.methodDescriptorDeleteTodo,
        callback);
    }
    return this.client_.unaryCall(
    this.hostname_ +
      '/todoPackage.Todo/DeleteTodo',
    request,
    metadata || {},
    this.methodDescriptorDeleteTodo);
  }

}

