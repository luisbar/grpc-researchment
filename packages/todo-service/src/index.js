const grpc = require('grpc');
const protoLoader = require('@grpc/proto-loader');
const path = require('path');
const controllers = require('./controllers');

const todoDefinition = protoLoader.loadSync(path.resolve(__dirname, '../../protos/todo.proto'), {});
const todoProto = grpc.loadPackageDefinition(todoDefinition);
const todoPackage = todoProto.todoPackage;

const server = new grpc.Server();
server.bind('0.0.0.0:3002', grpc.ServerCredentials.createInsecure());
global.todos = [];

server.addService(todoPackage.Todo.service, {
  ...controllers,
});

server.start();