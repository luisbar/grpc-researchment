const { TodoItem, DeleteTodoResponse } = require('../../../protos/todo_pb');

const deleteTodo = (call, callback) => {
  const { id } = call.request;

  const removedTodo = global.todos.find((todo) => todo.id == id);
  global.todos = global.todos.filter((todo) => todo.id != id);

  const response = new DeleteTodoResponse();

  if (removedTodo)
    response
    .setItem(new TodoItem()
      .setId(removedTodo.id)
      .setTitle(removedTodo.title)
      .setDescription(removedTodo.description)
    );

  callback(null, response.toObject());
};

module.exports = deleteTodo;