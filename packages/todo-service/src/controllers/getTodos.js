const { GetTodosResponse, TodoItem } = require('../../../protos/todo_pb');

const getTodos = (call, callback) => {
  const { limit = 10 } = call.request;
  
  const todoItems = global.todos.slice(0, limit).map(todo => new TodoItem()
    .setId(todo.id)
    .setTitle(todo.title)
    .setDescription(todo.description)
  );
  const response = new GetTodosResponse()
    .setItemsList(todoItems); 

  callback(null, {
    items: response.toObject().itemsList,
  });
};

module.exports = getTodos;