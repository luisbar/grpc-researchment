const { TodoItem, GetTodoResponse } = require('../../../protos/todo_pb');

const getTodo = (call, callback) => {
  const { id } = call.request;
  
  const todo = global.todos.find((todo) => todo.id == id);

  const response = new GetTodoResponse();

  if (todo)
    response
    .setItem(new TodoItem()
      .setId(todo.id)
      .setTitle(todo.title)
      .setDescription(todo.description)
    );

  callback(null, response.toObject());
};

module.exports = getTodo;