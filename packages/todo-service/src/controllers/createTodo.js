const { CreateTodoResponse } = require('../../../protos/todo_pb');

const createTodo = (call, callback) => {
  const { title, description } = call.request;
  const id = global.todos.length + 1;
  global.todos.push({
    id,
    title: `${title} ${id}`,
    description: `${description} ${id}`,
  });
  
  const response = new CreateTodoResponse()
  .setId(id)
  .setTitle(`${title} ${id}`)
  .setDescription(`${description} ${id}`);

  callback(null, response.toObject());
};

module.exports = createTodo;