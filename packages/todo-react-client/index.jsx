import React from 'react'
import ReactDOM from 'react-dom';
import { useState } from 'react';
import { TodoClient } from '../protos/TodoServiceClientPb';
import {
  CreateTodoRequest,
  GetTodosRequest,
  GetTodoRequest,
  DeleteTodoRequest,
} from '../protos/todo_pb';
const client = new TodoClient('http://localhost:3001');

const App = () => {
  const [todoIdToGet, setTodoIdToGet] = useState('');
  const [todoIdToDelete, setTodoIdToDelete] = useState('');
  const [createOneResponse, setCreateOneResponse] = useState('');
  const [getTodosResponse, setTodosResponse] = useState('');
  const [getTodoResponse, setGetTodoResponse] = useState('');
  const [deleteTodoResponse, setDeleteTodoResponse] = useState('');

  const createOne = async () => {
    try {
      const response = await client.createTodo(new CreateTodoRequest().setTitle('title').setDescription('description'));
      setCreateOneResponse(response);
    } catch (error) {
      console.log(error.message);
    }
  }

  const getTodos = async () => {
    try {
      const response = await client.getTodos(new GetTodosRequest());
      setTodosResponse(response);
    } catch (error) {
      console.log(error.message);
    }
  }

  const getTodo = async () => {
    try {
      const response = await client.getTodo(new GetTodoRequest().setId(todoIdToGet));
      setGetTodoResponse(response);
    } catch (error) {
      console.log(error.message);
    }
  }

  const deleteOne = async () => {
    try {
      const response = await client.deleteTodo(new DeleteTodoRequest().setId(todoIdToDelete));
      setDeleteTodoResponse(response);
    } catch (error) {
      console.log(error.message);
    }
  }

  const onChangeTodoIdToGet = ({ target: { value } }) => {
    setTodoIdToGet(() => value);
  };

  const onChangeTodoIdToDelete = ({ target: { value } }) => {
    setTodoIdToDelete(() => value);
  };

  return (
    <div
      style={{
        display: 'grid',
        gridRow: 2,
        gridColumn: 6,
        gridTemplateColumns: '1fr 1fr 1fr 1fr 1fr 1fr',
        gridTemplateRows: 'auto 50vh',
        gridColumnGap: 10,
        gridRowGap: 10
      }}
    >
      <button
        style={{
          gridColumnStart: 1
        }}
        onClick={createOne}
      >
        Create to do
      </button>
      <textarea
        style={{
          gridColumnStart: 1,
          gridRowStart: 2
        }}
        readOnly
        value={createOneResponse}
      />
      <button
        style={{
          gridColumnStart: 2
        }}
        onClick={getTodos}
      >
        Get Many
      </button>
      <textarea
        style={{
          gridColumnStart: 2,
          gridRowStart: 2
        }}
        readOnly
        value={getTodosResponse}
      />
      <button
        style={{
          gridColumnStart: 3
        }}
        onClick={getTodo}
      >
        Get One
      </button>
      <input
        style={{
          gridColumnStart: 4
        }}
        onChange={onChangeTodoIdToGet}
        type='text'
        placeholder='To do id'
        value={todoIdToGet}
      />
      <textarea
        style={{
          gridColumnStart: 3,
          gridColumnEnd: 5,
          gridRowStart: 2
        }}
        readOnly
        value={getTodoResponse}
      />
      <button
        style={{
          gridColumnStart: 5
        }}
        onClick={deleteOne}
      >
        Delete One
      </button>
      <input
        style={{
          gridColumnStart: 6
        }}
        onChange={onChangeTodoIdToDelete}
        type='text'
        placeholder='To do id'
        value={todoIdToDelete}
      />
      <textarea
        style={{
          gridColumnStart: 5,
          gridColumnEnd: 7,
          gridRowStart: 2
        }}
        readOnly
        value={deleteTodoResponse}
      />
    </div>
  );
}

const app = document.getElementById('app');
ReactDOM.render(<App />, app);