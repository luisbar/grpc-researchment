FROM node:16.5.0-alpine
WORKDIR /packages/protos
RUN npm init -y && npm i google-protobuf grpc-web
COPY ./packages/protos .