## How to run?

1. Install docker
2. Run using `npm start`
3. Open http://localhost:3000

## If you modify .proto file you have to regenerate web classes

1. Install `protobuf` and `protoc-gen-grpc-web` locally using Homebrew for Mac
2. Execute `./generateWebClientFromProto.sh` script

## Hints
- You can use [Bloomrpc](https://github.com/bloomrpc/bloomrpc) if you want to do request to the service without using the react client