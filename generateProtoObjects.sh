# Generate types for typescript
# yarn proto-loader-gen-types --grpcLib=@grpc/grpc-js --outDir=packages/protos/ packages/protos/todo.proto

protoc --proto_path=. \
--js_out=import_style=commonjs:. \
--grpc-web_out=import_style=typescript,mode=grpcwebtext:. \
./packages/protos/todo.proto